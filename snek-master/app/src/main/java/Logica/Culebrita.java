package Logica;

import android.graphics.Color;

public class Culebrita extends  Movimiento{
    public Culebrita(Snek snek){
        super(snek);
        this.segmentos[0][0] = 11;
        this.segmentos[0][1] = 5;
        this.segmentos[1][0] = 10;
        this.segmentos[1][1] = 5;
        this.segmentos[2][0] = 9;
        this.segmentos[2][1] = 5;
        this.segmentos[3][0] = 8;
        this.segmentos[3][1] = 5;
        this.color = Color.CYAN;
    }




}
