package Logica;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import Until.Movimientos;

public class Snek extends Observable {
    public final static int FILAS = 20;
    public final static int COLUMNAS = 10;
    int matriz[][] = new int[20][10];
    public Movimiento movimiento;

    /*public void fichaNueva(){
        this.movimiento = new Culebrita(this);
    }*/

    public void iniciarJuego() {
        this.movimiento = new Culebrita(this);
        ArrayList<Object> elemento = new ArrayList<Object>();
        elemento.add(this.movimiento.getSegmentos());
        elemento.add(this.movimiento.getColor());
        this.setChanged();
        this.notifyObservers(elemento);
        TimerTask timerTask = new TimerTask() {
            public void run() {
                if (movimiento.getDireccion() == Movimientos.DERECHA) {
                    moverFicha(Movimientos.DERECHA);
                } else if (movimiento.getDireccion() == Movimientos.IZQUIERDA) {
                    moverFicha(Movimientos.IZQUIERDA);
                } else if (movimiento.getDireccion() == Movimientos.ABAJO) {
                    moverFicha(Movimientos.ABAJO);
                } else if (movimiento.getDireccion() == Movimientos.ARRIBA) {
                    moverFicha(Movimientos.ARRIBA);
                }
            }
        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 1000);

    }

    public void moverFicha(String movimiento) {
        int [][]segmentosAnteriores = new int[4][2];
        for(int i=0; i<segmentosAnteriores.length; i++){
            segmentosAnteriores[i][0] = this.movimiento.getSegmentos()[i][0];
            segmentosAnteriores[i][1] = this.movimiento.getSegmentos()[i][1];
        }

        boolean hecho = false;

        if(movimiento == Movimientos.IZQUIERDA){
            hecho = this.movimiento.izquierda();
            this.movimiento.setDireccion(movimiento);
        }else if(movimiento == Movimientos.DERECHA){
            hecho = this.movimiento.derecha();
            this.movimiento.setDireccion(movimiento);
        }else if(movimiento == Movimientos.ABAJO){
            hecho = this.movimiento.abajo();
            this.movimiento.setDireccion(movimiento);
        }else if(movimiento == Movimientos.ARRIBA) {
            hecho = this.movimiento.arriba();
            this.movimiento.setDireccion(movimiento);
        }
        if(hecho){
            ArrayList<Object> elemento = new ArrayList<Object>();
            elemento.add(this.movimiento.getSegmentos());
            elemento.add(this.movimiento.getColor());
            elemento.add(segmentosAnteriores);
            this.setChanged();
            this.notifyObservers(elemento);
        }

    }



}
