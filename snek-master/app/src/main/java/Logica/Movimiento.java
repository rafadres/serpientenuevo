package Logica;

public abstract class Movimiento {
    protected Snek snek;
    protected int color,x,y;
    protected int segmentos[][] = new int[4][2];

    protected String direccion;

    public Movimiento(Snek snek){
        this.snek = snek;
    }

    public int[][] getSegmentos() {
        return segmentos;
    }

    public int getColor() {
        return color;
    }
    public String getDireccion(){
        return direccion;
    }
    public String setDireccion(String movimiento){
        return this.direccion=movimiento;
    }

    public boolean izquierda(){
        for(int i=0; i<this.segmentos.length; i++){
            if(this.segmentos[i][1] == 0){
                return false;
            }
        }
        arreglar();
        this.segmentos[0][1]--;
        return true;
    }

    public boolean derecha(){
        for(int i=0; i<this.segmentos.length; i++){
            if(this.segmentos[i][1] == Snek.COLUMNAS-1){
                return false;
            }
        }
        arreglar();
        this.segmentos[0][1]++;
        return true;
    }

    public boolean abajo(){
        for(int i=0; i<this.segmentos.length; i++){
            if(this.segmentos[i][0] == Snek.FILAS-1){
                return false;
            }
        }
        arreglar();
        this.segmentos[0][0]++;
        return true;
    }
    public boolean arriba(){
        for(int i=0; i<this.segmentos.length; i++){
            if(this.segmentos[i][0] == 0){
                return false;
            }
        }
        arreglar();
        this.segmentos[0][0]--;
        return true;
    }
    public void arreglar(){
        int aux2=0,aux21=0;
        for(int i=1; i<this.segmentos.length; i++){
            int aux= this.segmentos[i][1];
            int aux1=this.segmentos[i][0];
            if (i==1){
                this.segmentos[i][1]=this.segmentos[i-1][1];
                this.segmentos[i][0]=this.segmentos[i-1][0];
            }else{
                this.segmentos[i][1]=aux2;
                this.segmentos[i][0]=aux21;
            }
            aux2=aux;
            aux21=aux1;
        }
    }

}
