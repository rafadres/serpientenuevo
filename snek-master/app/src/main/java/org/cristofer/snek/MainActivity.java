package org.cristofer.snek;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import Controlador.Control;
import Logica.Snek;

public class MainActivity extends AppCompatActivity {
    private Snek snek;
    private Matriz matriz;
    private Control control;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.matriz = new Matriz(this);
        this.control = new Control(this);
        this.snek = new Snek();
        this.snek.addObserver(this.matriz);
        this.snek.iniciarJuego();
    }

    public void moverFicha(String movimiento) {
        this.snek.moverFicha(movimiento);
    }
    /*public void mover(String movimiento) {
        this.snek.mover(movimiento);
    }*/
}