package org.cristofer.snek;

import android.graphics.Color;
import android.graphics.Point;
import android.view.Display;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import Logica.Snek;

public class Matriz implements Observer {

    private GridLayout gridLayoutMatriz;
    private TextView textViewMatriz[][];
    private LinearLayout linearLayoutSnek;
    private MainActivity mainActivity;

    public Matriz(MainActivity mainActivity){
        this.mainActivity = mainActivity;
        this.textViewMatriz = new TextView[Snek.FILAS][Snek.COLUMNAS];
        this.gridLayoutMatriz = new GridLayout(this.mainActivity);
        this.gridLayoutMatriz.setColumnCount(10);
        this.gridLayoutMatriz.setRowCount(20);
        this.linearLayoutSnek = this.mainActivity.findViewById(R.id.linearLayoutSnek);
        Display display = this.mainActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        for(int i=0; i<Snek.FILAS; i++){
            for(int j=0; j<Snek.COLUMNAS; j++){
                this.textViewMatriz[i][j] = new TextView(this.mainActivity);
                this.textViewMatriz[i][j].setWidth((int)(width*0.7/10));
                this.textViewMatriz[i][j].setHeight((int)(width*0.7/10));
                this.textViewMatriz[i][j].setBackgroundColor(Color.GREEN);
                this.gridLayoutMatriz.addView(this.textViewMatriz[i][j]);
            }
        }
        this.linearLayoutSnek.addView(this.gridLayoutMatriz);
    }

    public void update(Observable o, Object arg) {
        ArrayList<Object> elemento = (ArrayList<Object>) arg;
        int segmentos[][] = (int[][])elemento.get(0);
        int color = (int)elemento.get(1);
        if(elemento.size() > 2){
            int segmentosAnteriores[][] = (int[][])elemento.get(2);
            for(int i=0; i<segmentosAnteriores.length; i++){
                this.textViewMatriz[segmentosAnteriores[i][0]][segmentosAnteriores[i][1]].setBackgroundColor(Color.GREEN);
            }
        }
        for(int i=0; i<segmentos.length; i++){
            this.textViewMatriz[segmentos[i][0]][segmentos[i][1]].setBackgroundColor(color);
        }
    }
}
